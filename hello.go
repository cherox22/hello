package main

import (
	"fmt"

	"github.com/cherox22/stringutil"
)

func main() {
	fmt.Printf(stringutil.Reverse("hello, world!\n"))
}
